package cr.ac.ucenfotec.cl;

import java.time.LocalDate;

public class PersonaJuridica extends Persona {
    private String representante;
    private String industria;


    public PersonaJuridica() {
    }

    public PersonaJuridica(String nombre, String identificacion, String direccion, int telefono, String representante, String industria) {
        super(nombre, identificacion, direccion, telefono);
        this.representante = representante;
        this.industria = industria;
    }

    public String getRepresentante() {
        return representante;
    }

    public void setRepresentante(String representante) {
        this.representante = representante;
    }

    public String getIndustria() {
        return industria;
    }

    public void setIndustria(String industria) {
        this.industria = industria;
    }

    @Override
    public String toString() {
        return  super.toString()+
                "representante='" + representante + '\'' +
                ", industria='" + industria + '\'' +
                '}';
    }
}
