package cr.ac.ucenfotec.ui;

import cr.ac.ucenfotec.cl.Persona;
import cr.ac.ucenfotec.cl.PersonaFisica;
import cr.ac.ucenfotec.cl.PersonaJuridica;

import java.time.LocalDate;

public class UI {
    public static void main(String[] args) {
        Persona persona1 = new Persona("Juan","123456","San Jose",22784606);
        PersonaJuridica personaJuridica1 = new PersonaJuridica("Rachel","123456","Heredia",868284,"Juanito H.","Carnes");
        PersonaFisica personaFisica1 = new PersonaFisica("Carlos","123456789","Cartago",7894561,"Hernandez",'H',35, LocalDate.of(1985,12,01));

        System.out.println("Persona: " +persona1);
        System.out.println("Persona fisica: "+personaFisica1);
        System.out.println("Persona juridica: "+personaJuridica1);

    }
}
